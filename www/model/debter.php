<?php

class Debtor {
	
	public static function index($id) {
		
		$db = Db::get_connection();
		$sql = "SELECT * FROM debter WHERE id_user=" . $id;
		$result = $db->prepare($sql);
		$result->execute();
		$debtors = $result->fetchAll();

		return $debtors;
	}

	public static function add($id, $forms) {
		
		$db = Db::get_connection();
		$sql = "INSERT INTO debter (surname, first_name, last_name, sex, `date`, inn, snils, pasport, id_user) VALUES (:surname, :first_name, :last_name, :sex, :dob, :inn, :snils, :pasport, :id_user)";
		$result = $db->prepare($sql);
		$result->bindParam(':surname', $forms['surname'], PDO::PARAM_STR);
		$result->bindParam(':first_name', $forms['first_name'], PDO::PARAM_STR);
		$result->bindParam(':last_name', $forms['last_name'], PDO::PARAM_STR);
		$result->bindParam(':sex', $forms['sex'], PDO::PARAM_STR);
		$result->bindParam(':dob', $forms['date'], PDO::PARAM_STR);
		$result->bindParam(':inn', $forms['inn'], PDO::PARAM_INT);
		$result->bindParam(':snils', $forms['snils'], PDO::PARAM_INT);
		$result->bindParam(':pasport', $forms['pasport'], PDO::PARAM_INT);
		$result->bindParam(':id_user', $id, PDO::PARAM_INT);
		$result = $result->execute();

		return $result;
	}

	public static function filling_form($id) {

		$db = Db::get_connection();
		$sql = 'SELECT * FROM debter WHERE id = :id';
		$result = $db->prepare($sql);
		$result->bindParam(':id', $id, PDO::PARAM_INT);
		$result->execute();

		$form_data = $result->fetch();
		return $form_data;
	}

	public static function change_info($id, $forms) {
		
		$db = Db::get_connection();
		$sql = "UPDATE debter SET surname=:surname, first_name=:first_name, last_name=:last_name, sex=:sex, `date`=:dob, inn=:inn, snils=:snils, pasport=:pasport WHERE id=" . $id;
		$result = $db->prepare($sql);
		$result->bindParam(':surname', $forms['surname'], PDO::PARAM_STR);
		$result->bindParam(':first_name', $forms['first_name'], PDO::PARAM_STR);
		$result->bindParam(':last_name', $forms['last_name'], PDO::PARAM_STR);
		$result->bindParam(':sex', $forms['sex'], PDO::PARAM_STR);
		$result->bindParam(':dob', $forms['date'], PDO::PARAM_STR);
		$result->bindParam(':inn', $forms['inn'], PDO::PARAM_INT);
		$result->bindParam(':snils', $forms['snils'], PDO::PARAM_INT);
		$result->bindParam(':pasport', $forms['pasport'], PDO::PARAM_INT);
		$result = $result->execute();

		return $result;
	}

	public static function delete_debetor($id) {
		$db = Db::get_connection();
		$sql = "DELETE FROM debter WHERE id=". $id;
		$result = $db->prepare($sql);
		$result->execute();

	}

}