<?php

class Cabinet {

	public static function filling_form() {

		$db = Db::get_connection();
		$sql = 'SELECT * FROM users WHERE id = :id';
		$id = $_SESSION['id'];
		$result = $db->prepare($sql);
		$result->bindParam(':id', $id, PDO::PARAM_INT);
		$result->execute();

		$form_data = $result->fetch();
		return $form_data;
	}

	public static function change($form) {

		$db = Db::get_connection();
		$sql = "SELECT * FROM users WHERE login = :login AND id <> :id";
		$result = $db->prepare($sql);
		$result->bindParam(':id', $_SESSION['id'], PDO::PARAM_INT);
		$result->bindParam(':login', $form['login'], PDO::PARAM_STR);
		$id = $_SESSION['id'];
		$user = $result->fetch();
		if($user) {
			if ($form['login']==$user["login"]) {
				
				$sql = "UPDATE users SET password=:password, surname=:surname, first_name=:first_name, last_name=:last_name, `index`=:index, address=:address WHERE id=$id";

				$result = $db->prepare($sql);
				$result->bindParam(':password', $form['password'],PDO::PARAM_STR);
				$result->bindParam(':surname', $form['surname'],PDO::PARAM_STR);
				$result->bindParam(':first_name', $form['first_name'],PDO::PARAM_STR);
				$result->bindParam(':last_name', $form['last_name'],PDO::PARAM_STR);
				$result->bindParam(':index', $form['index'],PDO::PARAM_INT);
				$result->bindParam(':address', $form['address'],PDO::PARAM_STR);
				$result = $result->execute();
				return $result;
			}
			return false;
		} else {
				$sql = "UPDATE users SET login=:login, password=:password, surname=:surname, first_name=:first_name, last_name=:last_name, `index`=:index, address=:address WHERE id=$id";

				$result = $db->prepare($sql);
				$result->bindParam(':login', $form['login'],PDO::PARAM_STR);
				$result->bindParam(':password', $form['password'],PDO::PARAM_STR);
				$result->bindParam(':surname', $form['surname'],PDO::PARAM_STR);
				$result->bindParam(':first_name', $form['first_name'],PDO::PARAM_STR);
				$result->bindParam(':last_name', $form['last_name'],PDO::PARAM_STR);
				$result->bindParam(':index', $form['index'],PDO::PARAM_INT);
				$result->bindParam(':address', $form['address'],PDO::PARAM_STR);
				$result = $result->execute();
				return $result;
		}
	}
}
