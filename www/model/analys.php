<?php

class Analys {
	
	public static function unzip() {
		$zip = new ZipArchive(); 
		$str = "reports/main.zip";
		if ($zip->open($str) === true) {
			$zip->extractTo("reports/");
			$zip->close();
		} else echo "Архива не существует!";
	}

	public static function index($id) {
		
		$db = Db::get_connection();
		$sql = "SELECT * FROM users WHERE id=" . $_SESSION['id'];
		$result = $db->prepare($sql);
		$result->execute();
		$data['users'] = $result->fetch();

		$sql = "SELECT * FROM debter WHERE id=" . $id;
		$result = $db->prepare($sql);
		$result->execute();
		$data['debtor'] = $result->fetch();

		$sql = "SELECT * FROM creditor WHERE id_debtor=" . $id;
		$result = $db->prepare($sql);
		$result->execute();
		$data['creditor'] = $result->fetchAll();

		$sql = "SELECT * FROM property WHERE id_debtor=" . $id;
		$result = $db->prepare($sql);
		$result->execute();
		$data['property'] = $result->fetchAll();

		$sql = "SELECT * FROM income WHERE id_debtor=" . $id;
		$result = $db->prepare($sql);
		$result->execute();
		$data['income'] = $result->fetchAll();

		return $data;
	}

	public static function change($data, $resulted) {
		$str = 'reports/word/document.xml';
		$xml = simplexml_load_file($str); 
		$ns = $xml->getNameSpaces(true); 
		$result = $xml->children($ns['w']);


		$counter = array();

		foreach ($result->body->p as $key) {

			$lol = array();
			$tmp = $key->r;
			foreach ($tmp as $r) {
				if ($r->t == 'debtor') {
					$r->t = $data['debtor']['surname'] . $data['debtor']['first_name'] . $data['debtor']['last_name'];
				}
				if ($r->t == 'user') {
					$r->t = $data['users']['surname'] . $data['users']['first_name'] . $data['users']['last_name'];
				}
				if ($r->t == 'index') {
					$r->t = $data['users']['index'];
				}
				if ($r->t == 'address') {
					$r->t = $data['users']['address'];
				}
				if ($r->t == 'sro') {
					$r->t = $data['users']['sro'];
				}
				if ($r->t == 'date') {
					$r->t = $data['debtor']['date'];
				}
				if ($r->t == 'passport') {
					$r->t = $data['debtor']['pasport'];
				}
				if ($r->t == 'snils') {
					$r->t = $data['debtor']['snils'];
				}
				if ($r->t == 'inn') {
					$r->t = $data['debtor']['inn'];
				}
				if ($r->t == 'propertypricesum') {
					$r->t = strval($resulted['property']['price']);
				}
				if ($r->t == 'creditordebtssum') {
					$r->t = strval($resulted['creditor']['debts']);
				}
				if ($r->t == 'creditordelaysum') {
					$r->t = strval($resulted['creditor']['delay']);
				}
				if ($r->t == 'incomepricesum') {
					$r->t = strval($resulted['income']['price']);
				}
				if ($r->t == 'creditorpaymantsum') {
					$r->t = strval($resulted['creditor']['paymant']);
				}
			}
		}

		$xml->asXML($str);
	}


	public static function download($name) {
    $file = (dir . "/reports/" . $name . ".docx");
    header ("Content-Type: application/octet-stream");
    header ("Accept-Ranges: bytes");
    header ("Content-Length: ".filesize($file));
    header ("Content-Disposition: attachment; filename=$name" . ".docx");
    readfile($file);
	}

}