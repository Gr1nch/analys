<?php

class Data {
	
	public static function index($id) {
		
		$db = Db::get_connection();
		$sql = "SELECT * FROM debter WHERE id=" . $id;
		$result = $db->prepare($sql);
		$result->execute();
		$data['debtor'] = $result->fetch();

		$sql = "SELECT * FROM creditor WHERE id_debtor=" . $id;
		$result = $db->prepare($sql);
		$result->execute();
		$data['creditor'] = $result->fetchAll();

		$sql = "SELECT * FROM property WHERE id_debtor=" . $id;
		$result = $db->prepare($sql);
		$result->execute();
		$data['property'] = $result->fetchAll();

		$sql = "SELECT * FROM income WHERE id_debtor=" . $id;
		$result = $db->prepare($sql);
		$result->execute();
		$data['income'] = $result->fetchAll();

		return $data;
	}

	public static function add_creditor($id, $forms) {

		$db = Db::get_connection();
		$sql = "INSERT INTO creditor (name, debts, delay, paymant, id_debtor) VALUES (:name, :debts, :delay, :paymant, :id_debtor)";
		$result = $db->prepare($sql);
		$result->bindParam(':name', $forms['name'], PDO::PARAM_STR);
		$result->bindParam(':debts', $forms['debts'], PDO::PARAM_STR);
		$result->bindParam(':delay', $forms['delay'], PDO::PARAM_STR);
		$result->bindParam(':paymant', $forms['m-paymant'], PDO::PARAM_STR);
		$result->bindParam(':id_debtor', $id, PDO::PARAM_INT);
		$result = $result->execute();

		return $result;
	}

	public static function add_property($id, $forms) {
		
		$db = Db::get_connection();
		$sql = "INSERT INTO property (name, count, unit, id_debtor, price, confirmat) VALUES (:name, :count, :unit, :id_debtor, :price, :conf)";
		$result = $db->prepare($sql);
		$result->bindParam(':name', $forms['name'], PDO::PARAM_STR);
		$result->bindParam(':count', $forms['count'], PDO::PARAM_STR);
		$result->bindParam(':unit', $forms['unit'], PDO::PARAM_STR);
		$result->bindParam(':price', $forms['price'], PDO::PARAM_STR);
		$result->bindParam(':conf', $forms['conf'], PDO::PARAM_STR);
		$result->bindParam(':id_debtor', $id, PDO::PARAM_INT);
		$result = $result->execute();

		return $result;
	}

	public static function add_income($id, $forms) {
		$db = Db::get_connection();
		$sql = "INSERT INTO income (name, price, id_debtor) VALUES (:name, :price, :id_debtor)";
		$result = $db->prepare($sql);
		$result->bindParam(':name', $forms['name'], PDO::PARAM_STR);
		$result->bindParam(':price', $forms['price'], PDO::PARAM_STR);
		$result->bindParam(':id_debtor', $id, PDO::PARAM_INT);
		$result = $result->execute();

		return $result;
	}

	public static function filling_creditor($id) {
		
		$db = Db::get_connection();
		$sql = "SELECT * FROM creditor WHERE id=" . $id;
		$result = $db->prepare($sql);
		$result->execute();
		$creditor = $result->fetch();

		return $creditor;
	}

	public static function filling_property($id) {
		
		$db = Db::get_connection();
		$sql = "SELECT * FROM property WHERE id=" . $id;
		$result = $db->prepare($sql);
		$result->execute();
		$property = $result->fetch();

		return $property;
	}

	public static function filling_income($id) {
		
		$db = Db::get_connection();
		$sql = "SELECT * FROM income WHERE id=" . $id;
		$result = $db->prepare($sql);
		$result->execute();
		$property = $result->fetch();

		return $property;
	}

	public static function change_creditor($id, $forms) {
		
		$db = Db::get_connection();
		$sql = "UPDATE creditor SET name=:name, debts=:debts, delay=:delay, paymant=:paymant WHERE id=" . $id;
		$result = $db->prepare($sql);
		$result->bindParam(':name', $forms['name'], PDO::PARAM_STR);
		$result->bindParam(':debts', $forms['debts'], PDO::PARAM_STR);
		$result->bindParam(':delay', $forms['delay'], PDO::PARAM_STR);
		$result->bindParam(':paymant', $forms['m-paymant'], PDO::PARAM_STR);
		$result = $result->execute();

		return $result;
	}

	public static function change_property($id, $forms) {
		
		$db = Db::get_connection();
		$sql = "UPDATE property SET name=:name, count=:count, unit=:unit, confirmat=:conf WHERE id=" . $id;
		$result = $db->prepare($sql);
		$result->bindParam(':name', $forms['name'], PDO::PARAM_STR);
		$result->bindParam(':count', $forms['count'], PDO::PARAM_STR);
		$result->bindParam(':unit', $forms['unit'], PDO::PARAM_STR);
		$result->bindParam(':conf', $forms['conf'], PDO::PARAM_STR);
		$result = $result->execute();

		return $result;
	}

	public static function change_income($id, $forms) {
		
		$db = Db::get_connection();
		$sql = "UPDATE income SET name=:name, price=:price WHERE id=" . $id;
		$result = $db->prepare($sql);
		$result->bindParam(':name', $forms['name'], PDO::PARAM_STR);
		$result->bindParam(':price', $forms['price'], PDO::PARAM_STR);
		$result = $result->execute();

		return $result;
	}
}