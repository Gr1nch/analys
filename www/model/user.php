<?php

class User {

  public static function sql_inner($forms) {
    $db = Db::get_connection();
    $db->exec('SET CHARACTER SET utf8');

    $result = $db->prepare($forms[0]);
    // print_r($db->errorInfo());
    $counter = count($forms);
    switch ($counter) {
      case 2:
        $result->bindParam(':login', $forms['login'], PDO::PARAM_INT);
        break;
        
      case 3:
        $result->bindParam(':login', $forms['login'], PDO::PARAM_STR);
        $result->bindParam(':password', $forms['password'], PDO::PARAM_STR);
        break;
      
      case 9:
        $result->bindParam(':login', $forms['login'], PDO::PARAM_STR);
        $result->bindParam(':password', $forms['password'], PDO::PARAM_STR);
        $result->bindParam(':surname', $forms['surname'], PDO::PARAM_STR);
        $result->bindParam(':first_name', $forms['first_name'], PDO::PARAM_STR);
        $result->bindParam(':last_name', $forms['last_name'], PDO::PARAM_STR);
        $result->bindParam(':index', $forms['index'], PDO::PARAM_INT);
        $result->bindParam(':address', $forms['address'], PDO::PARAM_STR);
        $result->bindParam(':sro', $forms['sro'], PDO::PARAM_STR);
        break;
      }

      $result->execute();

      return $result;
    }

  public static function login($forms) {

    $sql = "SELECT * FROM users WHERE login = :login and password = :password";
    array_unshift($forms, $sql);
    array_pop($forms);
    $result = User::sql_inner($forms);

    $user = $result->fetch();
    if ($user) {
      return $user['id'];
    } else return false;
  }

  public static function auth($id_user) {
    $_SESSION['id'] = $id_user;
  }

  public static function register($forms) {

    $sql = "SELECT * FROM users WHERE login = :login";

    $tmp[] = $forms["login"];
    array_unshift($tmp, $sql);
    array_pop($forms);
    $result = User::sql_inner($tmp);

    $user = $result->fetch();
    if ($user) {
      return false;
    } else {
      $sql = "INSERT INTO users (login, password, surname, first_name, last_name, `index`, address, sro) VALUES (:login, :password, :surname, :first_name, :last_name, :index, :address, :sro)";
      array_unshift($forms, $sql);
      $_SESSION['forms'] = $forms;
      $result = User::sql_inner($forms);
      return $result;
    }
  }
}
