<?php

class Word extends ZipArchive{

    // Файлы для включения в архив
    private $files;

    // Путь к шаблону
    public $path;

    public function __construct($filename){

      // Путь к шаблону
      $this->path = dir . '/reports/';

      // Если не получилось открыть файл, то жизнь бессмысленна.
      if ($this->open($filename, ZipArchive::CREATE) !== TRUE) {
        die("Unable to open <$filename>\n");
      }

      // Структура документа
      $this->files = array(
        "word/_rels/document.xml.rels",
        "word/theme/theme1.xml",
        "word/fontTable.xml",
        "word/settings.xml",
        "word/styles.xml",
        "word/document.xml",
        "word/webSettings.xml",
        "word/endnotes.xml",
        "word/footnotes.xml",
        "word/numbering.xml",
        "customXml/_rels/item1.xml.rels",
        "customXml/item1.xml",
        "customXml/itemProps1.xml",
        "_rels/.rels",
        "docProps/app.xml",
        "docProps/core.xml",
        "[Content_Types].xml" );

      // Добавляем каждый файл в цикле
      foreach( $this->files as $f ) {
        $result = $this->addFile($this->path . $f, $f);
      }
    }

    // Упаковываем архив
    public function create(){

      $this->close();
    }


}