<?php

require_once dir . '/model/data.php';

class Data_controller {
	
	public function action_index($par) {
		$id = $_SESSION['id_debtor'] = $par[0];

		$data = Data::index($id);

		include_once dir . '/view/data/index.php';
		return true;
	}

	public function action_add_creditor() {
		
		$forms = $_POST;
		$id = $_SESSION['id_debtor'];
		if (isset($_POST['add-creditor'])) {
			$result = Data::add_creditor($id, $forms);
			if ($result) {
				header('Location: /debtor-data/' . $id);
			} else echo "error";
		}
		include_once dir . '/view/data/creditor.php';
		return true;
	}

	public function action_add_property() {
		
		$forms = $_POST;
		$id = $_SESSION['id_debtor'];
		if (isset($_POST['add-property'])) {
			if (empty($forms['conf'])) {
				$forms['conf'] = "0";
			}
			$result = Data::add_property($id, $forms);
			if ($result) {
				header('Location: /debtor-data/' . $id);
			} else echo "error";
		}
		include_once dir . '/view/data/property.php';
		return true;
	}

	public function action_add_income() {
		
		$forms = $_POST;
		$id = $_SESSION['id_debtor'];
		if (isset($_POST['add-income'])) {
			$result = Data::add_income($id, $forms);
			if ($result) {
				header('Location: /debtor-data/' . $id);
			} else echo "error";
		}
		include_once dir . '/view/data/income.php';
		return true;
	}

	public function action_change_creditor($par) {
		
		$id = $par[0];
		$forms = $_POST;
		$creditor = Data::filling_creditor($id);
		if (isset($_POST['change-creditor'])) {
			$result = Data::change_creditor($id, $forms);
			if ($result) {
				header('Location: /debtor-data/' . $_SESSION['id_debtor']);
			}
		}
		include_once dir . '/view/data/creditor.php';
		return true;
	}

	public function action_change_property($par) {
		
		$id = $par[0];
		$forms = $_POST;
		$property = Data::filling_property($id);
		if (isset($_POST['change-property'])) {
			if (empty($forms['conf'])) {
				$forms['conf'] = "0";
			}
			$result = Data::change_property($id, $forms);
			if ($result) {
				header('Location: /debtor-data/' . $_SESSION['id_debtor']);
			}
		}
		include_once dir . '/view/data/property.php';
		return true;
	}

	public function action_change_income($par) {
		
		$id = $par[0];
		$forms = $_POST;
		$income = Data::filling_income($id);
		if (isset($_POST['change-income'])) {
			$result = Data::change_income($id, $forms);
			if ($result) {
				header('Location: /debtor-data/' . $_SESSION['id_debtor']);
			}
		}
		include_once dir . '/view/data/income.php';
		return true;
	}
}