<?php

require_once dir . '/model/debter.php';

class Debtor_controller {
	
	public function action_index() {
		
		$id = $_SESSION['id'];
		$debtors = Debtor::index($id);
		include_once dir . '/view/debtor/index.php';
		return true;
	}

	public function action_add() {

		$form = $_POST;
		$id = $_SESSION['id'];
		if (isset($_POST['debter'])) {
			$result = Debtor::add($id, $form);
			if ($result) {
				header('Location: /debtor');
			} else {
				echo "Ошибка ввода данных";
			}
		}
		include_once dir . '/view/debtor/add.php';
		return true;
	}

	public function action_change_info($par) {
		$id = $par[0];
		$forms = $_POST;
		$form_data = Debtor::filling_form($id);
		if (isset($_POST['debter'])) {
			$result = Debtor::change_info($id, $forms);
			if ($result) {
				header('Location: /debtor');
			} else echo "error";
		}

		include_once dir . '/view/debtor/change_info.php';
		return true;
	}

	public function action_delete($par) {
		$id = $par[0];

		Debtor::delete_debetor($id);
		header('Location: /debtor');
		return true;
	}
}
