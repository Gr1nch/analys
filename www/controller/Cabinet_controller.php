<?php

require_once dir . '/model/cabinet.php';

class Cabinet_controller {
	
	public function action_index() {
		if (isset($_SESSION['id'])) {
			
			include_once dir . '/view/cabinet/index.php';
		} else header('Location: /');

		return true;
	}

	public function action_change() {
		if (isset($_SESSION['id'])) {
			$form_data = Cabinet::filling_form();

			$form = array();
			$errors = false;

			if (isset($_POST['change'])) {
				$form = $_POST;

				$result = Cabinet::change($form, $form_data['login']);
				if ($result) {
					$errors[] = "Замена данных успешно произведена.";
					header('Location: /cabinet/change');
				} else {
					$errors[] = "Указанный логин уже существует.";
				}
			}

			include_once dir . '/view/cabinet/change.php';
			
		} else header('Location: /');

		return true;
	}
}
