<?php

require_once dir . '/model/user.php';

class User_controller {
  
  public function action_login() {
    $forms = array();
    $errors = false;

    if (isset($_POST['log_in'])) {
      $forms = $_POST;

      $user = User::login($forms);
      if ($user == false) {
        $errors[] = "Неверно указан логин или пароль";
      } else {
        User::auth($user);
        header('Location: /');
      }
    }

    include_once dir . '/view/user/login.php';
    return true;
  }

  public function action_register() {

    $forms = array();
    $errors = false;

    if (isset($_POST['check_out'])) {
      $forms = $_POST;

      $result = User::register($forms);
      if ($result) {
        header('Location: /login');
      } else {
        $errors[] = "Указанный логин уже существует";
      }
    }

    include_once dir . '/view/user/register.php';
    return true;
  }

  public function action_logout() {
    unset($_SESSION['id']);
    header('Location: /');
    return true;
  }
}