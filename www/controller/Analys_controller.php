<?php

include_once dir . '/model/analys.php';
include_once dir . '/model/test.php';

class Analys_controller {
	
	public function action_index($par) {
		
		$id = $_SESSION['id_debtor'] = $par[0];
		$data = Analys::index($id);

		$result = array();
		$result['property']['price'] = $data['property'][0]['price'];
		foreach ($data['property'] as $property) {
			$result['property']['price'] += $property['price'];
		}
		$result['creditor']['debts'] = $data['creditor'][0]['debts'];
		$result['creditor']['delay'] = $data['creditor'][0]['delay'];
		$result['creditor']['paymant'] = $data['creditor'][0]['paymant'];
		foreach ($data['creditor'] as $creditor) {
			$result['creditor']['debts'] += $creditor['debts'];
			$result['creditor']['delay'] += $creditor['delay'];
			$result['creditor']['paymant'] += $creditor['paymant'];
		}
		$result['income']['price'] = $data['income'][0]['price'];
		foreach ($data['income'] as $income) {
			$result['income']['price'] += $income['price'];
		}
		// echo $result['property']['price'];

		Analys::unzip();
		Analys::change($data, $result);

		$str = "reports/" . $_SESSION['id_debtor'] . ".docx";
		$w = new Word($str);
		$w->create();
		include_once dir . '/view/analys/index.php';
		return true;
	}

	public function action_download($par) {
		
		Analys::download($par[0]);
		return true;
	}
}