<?php include_once dir . '/view/layout/header.php'; ?>

<a href="/add">Добавить должника</a>

<h1>Список должников</h1>

<?php if (!empty($debtors)): ?>
<table>
	<tr class="header-table">
		<td>Фамилия</td>
		<td>Имя</td>
		<td>Отчество</td>
		<td>Пол</td>
		<td>Дата рождения</td>
		<td>ИНН</td>
		<td>Снилс</td>
		<td>Номер паспорта</td>
		<td>Изменить общую информацию должника</td>
		<td>Удалить должника</td>
		<td>Добавить, изменить данные должника</td>
	</tr>
<?php foreach ($debtors as $debtor): ?>
	<?php 
	$debtor['sex'] == "male" ? $debtor['sex'] = "Мужской" : $debtor['sex'] = "Женский";
	?>
	<tr class="content-table">
		<td><?php echo $debtor['surname'] ?></td>
		<td><?php echo $debtor['first_name'] ?></td>
		<td><?php echo $debtor['last_name'] ?></td>
		<td><?php echo $debtor['sex'] ?></td>
		<td><?php echo $debtor['date'] ?></td>
		<td><?php echo $debtor['inn'] ?></td>
		<td><?php echo $debtor['snils'] ?></td>
		<td><?php echo $debtor['pasport'] ?></td>
		<td><a href="/change-infodebtor/<?php echo $debtor['id']; ?>">Изменить</a></td>
		<td><a href="/delete-debtor/<?php echo $debtor['id']; ?>">Удалить</a></td>
		<td><a href="/debtor-data/<?php echo $debtor['id']; ?>">Данные</a></td>
		<td><a href="/analys/<?php echo $debtor['id']; ?>">Анализ</a></td>
	</tr>
<?php endforeach; ?>
</table>
<?php endif; ?>