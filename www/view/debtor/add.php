<?php include_once dir . '/view/layout/header.php'; ?>

<h1>Добавить должника</h1>

<form action="" method="post">
	<input type="text" name="surname" placeholder="Фамилия">
	<input type="text" name="first_name" placeholder="Имя">
	<input type="text" name="last_name" placeholder="Отчество">
	<select name="sex" id="sex">
		<option value="male">Мужской</option>
		<option value="female">Женский</option>
	</select>
	<input type="date" name="date" placeholder="Дата рождения">
	<input type="text" name="inn" maxlength="12" placeholder="ИНН">
	<input type="text" name="snils" maxlength="11" placeholder="Снилс">
	<input type="text" name="pasport" placeholder="Номер паспорта">
	<input type="submit" name="debter">
</form>