<?php include_once dir . '/view/layout/header.php'; ?>

<form action="" method="post">
	<input type="text" name="surname" value="<?php echo $form_data['surname'] ?>">
	<input type="text" name="first_name" value="<?php echo $form_data['first_name'] ?>">
	<input type="text" name="last_name" value="<?php echo $form_data['last_name'] ?>">
	<select name="sex" id="sex">
	<?php if ($form_data['sex'] == "male"): ?>
		<option value="male" selected>Мужской</option>
		<option value="female">Женский</option>
	<?php else: ?>
		<option value="male">Мужской</option>
		<option value="female" selected>Женский</option>
	<?php endif; ?>
	</select>
	<input type="date" name="date" value="<?php echo $form_data['date'] ?>" >
	<input type="text" name="inn" maxlength="12" value="<?php echo $form_data['inn'] ?>">
	<input type="text" name="snils" maxlength="11" value="<?php echo $form_data['snils'] ?>">
	<input type="text" name="pasport" value="<?php echo $form_data['pasport'] ?>">
	<input type="submit" name="debter">
</form>