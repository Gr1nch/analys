<?php include_once dir . '/view/layout/header.php'; ?>

<?php include_once dir . '/view/data/add.php'; ?>

<h1>Данные <?php echo $data['debtor']['surname'] . " " . $data['debtor']['first_name']; ?></h1>

<?php if (!empty($data['creditor'])): ?>
<h3>Кредиторы</h3>
<table>
	<tr class="header-table">
		<td>Название кредитора</td>
		<td>Размер задолженности</td>
		<td>Размер просрочки</td>
		<td>Ежемесячный платеж</td>
		<td>Изменить</td>
	</tr>
<?php foreach ($data['creditor'] as $creditor): ?>
	<tr class="content-table content-table-data">
		<td><?php echo $creditor['name']; ?></td>
		<td><?php echo $creditor['debts']; ?></td>
		<td><?php echo $creditor['delay']; ?></td>
		<td><?php echo $creditor['paymant']; ?></td>
		<td><a href="/change-data-creditor/<?php echo $creditor['id']; ?>">Изменить</a></td>
	</tr>
<?php endforeach; ?>
</table>
<?php endif; ?>

<?php if (!empty($data['property'])): ?>
<h3>Имущество</h3>
<table>
	<tr class="header-table">
		<td>Наименование имущества</td>
		<td>Количество</td>
		<td>Единица измерения</td>
		<td>Цена</td>
		<td>Подлежит реализации</td>
		<td>Изменить</td>
	</tr>
<?php foreach ($data['property'] as $property): ?>
	<?php $property['unit'] == "thing" ? $property['unit'] = "Штука" : $property['unit'] = "Кв/м"; ?>
	<tr class="content-table content-table-data">
		<td><?php echo $property['name']; ?></td>
		<td><?php echo $property['count']; ?></td>
		<td><?php echo $property['unit']; ?></td>
		<td><?php echo $property['price']; ?></td>
		<td>
		<?php if ($property['confirmat'] == "1") {
			echo "Подлежит реализации";
		} else {
			echo "Не подлежит реализации";
		}
		?>
		</td>
		<td><a href="/change-data-property/<?php echo $property['id']; ?>">Изменить</a></td>
	</tr>
<?php endforeach; ?>
</table>
<?php endif; ?>

<?php if (!empty($data['income'])): ?>
<h3>Доходы</h3>
<table>
	<tr class="header-table">
		<td>Источник дохода</td>
		<td>Сумма</td>
		<td>Изменить</td>
	</tr>
<?php foreach ($data['income'] as $income): ?>
	<tr class="content-table content-table-data">
		<td><?php echo $income['name']; ?></td>
		<td><?php echo $income['price']; ?></td>
		<td><a href="/change-data-income/<?php echo $income['id']; ?>">Изменить</a></td>
	</tr>
<?php endforeach; ?>
</table>
<?php endif; ?>