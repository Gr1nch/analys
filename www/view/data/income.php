<?php 
include_once dir . '/view/layout/header.php';
include_once dir . '/view/data/add.php';
?>

<?php if (isset($income)): ?>
<h3>Измените необходимые данные доходов</h3>
<form action="" method="post">
	<input type="text" name="name" value="<?php echo $income['name']; ?>">
	<input type="text" name="price" value="<?php echo $income['price']; ?>">
	<input type="submit" name="change-income">
</form>
<?php else: ?>
<h3>Введите новые данные о доходах</h3>
<form action="" method="post">
	<input type="text" name="name" placeholder="Источник доходов">
	<input type="text" name="price" placeholder="Сумма">
	<input type="submit" name="add-income">
</form>
<?php endif; ?>