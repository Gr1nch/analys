<?php 
include_once dir . '/view/layout/header.php';
include_once dir . '/view/data/add.php';
?>

<?php if (isset($property)): ?>
<h3>Измените необходиме данные взимаемого имущества</h3>
<form action="" method="post">
	<input type="text" name="name" value="<?php echo $property['name']; ?>">
	<input type="text" name="count" value="<?php echo $property['count']; ?>">
	<select name="unit">
	<?php if ($property['unit'] == "sqm"): ?>
		<option value="sqm" selected>Кв/м</option>
		<option value="thing">Шт</option>
	<?php else: ?>
		<option value="sqm">Кв/м</option>
		<option value="thing" selected>Шт</option>
	<?php endif; ?>
	</select>
	<input type="text" name="price" value="<?php echo $property['price']; ?>">
	Подлеж. реализации
	<?php if ($property['confirmat'] == "1"): ?>
	<input class="check" type="checkbox" name="conf" value="1" checked>
	<?php else: ?>
	<input class="check" type="checkbox" name="conf" value="1">
	<?php endif; ?>
	<input type="submit" name="change-property">
</form>
<?php else: ?>
<h3>Введите новые данные взимаемого имущества</h3>
<form action="" method="post">
	<input type="text" name="name" placeholder="Наименование">
	<input type="text" name="count" placeholder="Измерение">
	<select name="unit">
		<option value="sqm">Кв/м</option>
		<option value="thing">Шт</option>
	</select>
	<input type="text" name="price" placeholder="Цена">Подлеж. реализации
	<input class="check" type="checkbox" name="conf" value="1">
	<input type="submit" name="add-property">
</form>

<?php endif; ?>