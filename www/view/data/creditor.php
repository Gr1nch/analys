<?php 
include_once dir . '/view/layout/header.php';
include_once dir . '/view/data/add.php';
?>

<?php if (isset($creditor)): ?>
<h3>Измените необходиме данные кредиторской задолженности</h3>
<form action="" method="post">
	<input type="text" name="name" value="<?php echo $creditor['name']; ?>">
	<input type="text" name="debts" value="<?php echo $creditor['debts']; ?>">
	<input type="text" name="delay" value="<?php echo $creditor['delay']; ?>">
	<input type="text" name="m-paymant" value="<?php echo $creditor['paymant']; ?>">
	<input type="submit" name="change-creditor">
</form>
<?php else: ?>
<h3>Введите новые данные кредиторской задолженности</h3>
<form action="" method="post">
	<input type="text" name="name" placeholder="Наименование">
	<input type="text" name="debts" placeholder="Размер задолженности">
	<input type="text" name="delay" placeholder="Размер просрочки">
	<input type="text" name="m-paymant" placeholder="Ежемесяынй платеж">
	<input type="submit" name="add-creditor">
</form>
<?php endif; ?>