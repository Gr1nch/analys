<?php include_once dir . '/view/layout/header.php'; ?>

<body>
	<?php if ($errors) {
		echo "<p>$errors[0]</p>";
	}
?>
	<form action="" method="post">
		Логин: <input type="text" name="login" value="<?php echo $form_data['login']; ?>">
		Пароль: <input type="text" name="password" value="<?php echo $form_data['password']; ?>">
		Фамилия: <input type="text" name="surname" value="<?php echo $form_data['surname']; ?>">
		Имя: <input type="text" name="first_name" value="<?php echo $form_data['first_name']; ?>">
		Отчество: <input type="text" name="last_name" value="<?php echo $form_data['last_name']; ?>">
		Индекс: <input type="text" name="index" value="<?php echo $form_data['index']; ?>" maxlength="6">
		Адрес: <input type="text" name="address" value="<?php echo $form_data['address']; ?>">
		<input type="submit" name="change">
	</form>
</body>

<?php include_once dir . '/view/layout/footer.php'; ?>
