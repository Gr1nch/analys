<?php 

return array(
	'^debtor$' => 'debtor/index',
	'^add$' => 'debtor/add',
	'^change-infodebtor/([0-9.]+)' => 'debtor/change_info/$1',
	'^delete-debtor/([0-9.]+)' => 'debtor/delete/$1',
	'^debtor-data/([0-9.]+)' => 'data/index/$1',
	'^add-creditor$' => 'data/add_creditor',
	'^add-property$' => 'data/add_property',
	'^add-income$' => 'data/add_income',
	'^change-data-creditor/([0-9.]+)' => 'data/change_creditor/$1',
	'^change-data-property/([0-9.]+)' => 'data/change_property/$1',
	'^change-data-income/([0-9.]+)' => 'data/change_income/$1',
	'^register$' => 'user/register',
	'^login$' => 'user/login',
	'^logout$' => 'user/logout',
	'^cabinet$' => 'cabinet/index',
	'^cabinet/change$' => 'cabinet/change',
	'^analys/([0-9.]+)' => 'analys/index/$1',
	'^download/([0-9.]+)' => 'analys/download/$1',
	'^preview/([0-9.]+)' => 'analys/preview/$1',
	'^$' => 'index/index'
);