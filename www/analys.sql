-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Июн 14 2016 г., 00:15
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `analys`
--

-- --------------------------------------------------------

--
-- Структура таблицы `creditor`
--

CREATE TABLE IF NOT EXISTS `creditor` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `debts` varchar(20) NOT NULL,
  `delay` varchar(20) NOT NULL,
  `paymant` varchar(20) NOT NULL,
  `id_debtor` int(11) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `creditor`
--

INSERT INTO `creditor` (`id`, `name`, `debts`, `delay`, `paymant`, `id_debtor`) VALUES
(1, 'Банк', '123', '321', '5', 1),
(2, 'лол', '123', '32', '234', 2),
(3, 'ОООО', '1222222', '11121', '12', 6),
(4, 'банк', '1500', '2000', '300', 5),
(5, 'банк', '2000', '3000', '500', 5),
(6, 'шкаф', '1500', '3000', '234', 7);

-- --------------------------------------------------------

--
-- Структура таблицы `debter`
--

CREATE TABLE IF NOT EXISTS `debter` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `surname` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `sex` varchar(10) NOT NULL,
  `date` date NOT NULL,
  `inn` int(12) NOT NULL,
  `snils` int(11) NOT NULL,
  `pasport` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `path_analys` text NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `debter`
--

INSERT INTO `debter` (`id`, `surname`, `first_name`, `last_name`, `sex`, `date`, `inn`, `snils`, `pasport`, `id_user`, `path_analys`) VALUES
(1, 'Лучко', 'Александр', '', 'male', '0000-00-00', 0, 0, 0, 0, ''),
(2, 'Кижеватова', 'Алина', 'Игоревна', 'female', '2016-06-01', 2147483647, 2147483647, 123456, 0, ''),
(3, 'Алина', '', '', 'male', '0000-00-00', 0, 0, 0, 0, ''),
(4, 'Кижеватова', 'Алина', 'Лох', 'female', '2016-06-24', 2147483647, 2147483647, 4564564, 0, ''),
(5, 'Лучко', 'Александр', 'Александрович', 'male', '2016-06-09', 2147483647, 2147483647, 123456, 2, ''),
(6, 'Алина', 'Fkbyf', 'Trh[rt;h[r', 'female', '2016-06-12', 889, 8080, 4564564, 3, ''),
(7, 'гп', 'гъ', 'гр', 'male', '2016-06-01', 12345, 2147483647, 123457, 2, '');

-- --------------------------------------------------------

--
-- Структура таблицы `income`
--

CREATE TABLE IF NOT EXISTS `income` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `price` varchar(20) NOT NULL,
  `id_debtor` int(11) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `income`
--

INSERT INTO `income` (`id`, `name`, `price`, `id_debtor`) VALUES
(1, 'Пенсия', '6000', 5),
(2, 'ЗП ', '666', 6),
(3, 'банк', '6000', 7);

-- --------------------------------------------------------

--
-- Структура таблицы `property`
--

CREATE TABLE IF NOT EXISTS `property` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `count` varchar(20) NOT NULL,
  `unit` varchar(10) NOT NULL,
  `price` varchar(20) NOT NULL,
  `confirmat` text NOT NULL,
  `id_debtor` int(11) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `property`
--

INSERT INTO `property` (`id`, `name`, `count`, `unit`, `price`, `confirmat`, `id_debtor`) VALUES
(1, 'Диван', '2', 'sqm', '', '2', 1),
(2, 'шкаф', '5', 'sqm', '', '0', 2),
(3, 'пальто', '1', 'thing', '', '1', 6),
(4, 'Диван', '2', 'thing', '2000', '0', 5),
(5, 'шкаф', '2', 'thing', '1500', '1', 5),
(6, 'стол', '5', 'sqm', '2000', '0', 5),
(7, 'стол', '5', 'thing', '2000', '0', 5),
(8, 'стол', '5', 'thing', '2000', '1', 5),
(9, 'Диван', '5', 'sqm', '1500', '0', 5),
(10, 'ркрек', 'ркерк', 'thing', 'рап', '1', 6);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `surname` varchar(20) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `index` int(11) NOT NULL,
  `address` text NOT NULL,
  `sro` varchar(50) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `surname`, `first_name`, `last_name`, `index`, `address`, `sro`) VALUES
(1, 'Gr1nch', '123', 'Лучко', 'Александр', 'Александрович', 666666, '', ''),
(2, 'grinch', '123', ',fdj', 'lknfdl', 'lkbfdl', 555555, 'чайковского ', ''),
(3, '1', '12345', 'Rb;tdfnjdf', 'Fkbyf', 'Trh[rt;h[r', 4, 'hgght', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
